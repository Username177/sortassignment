package GroupSorter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.InvalidClassException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Cash
 */
public class DefaultGroup implements Group {
    List<Row> rows = new LinkedList<>();
    
    @Override
    public int Size() {
        return rows.size();
    }
    
    @Override
    public void AddRow(Row row) {
        rows.add(row);
    }
    
    @Override
    public void RemoveRow(int position) {
        rows.remove(position);
    }
    
    @Override
    public Row GetRow(int position) {
        return rows.get(position);
    }

    @Override
    public void Include(Group group) throws InvalidClassException {
        if(group.getClass() != DefaultGroup.class) {
            throw new InvalidClassException("Разные типы групп не могут быть объединены");
        }
        
        Iterator<Row> iterator = ((DefaultGroup)group).Iterator();
        while(iterator.hasNext()) {
            rows.add(iterator.next());
        }
    }
    
    public Iterator<Row> Iterator() {
        return rows.iterator();
    }

    @Override
    public Row[] GetRows() {
        if(rows == null) {
            return null;
        }
        
        Row[] array = new Row[rows.size()];
        rows.toArray(array);
        
        return array;
    }
}
