package GroupSorter;



public interface File {
    boolean OpenForReading(final String fileName);
    boolean OpenForWriting(final String fileName, boolean append);
    String  Read(int length);
    String  ReadLine();
    boolean Write(final String data);
    boolean Seek(long index);
    void    Close();
    long    GetLength();
    String  GetFileName();
    String  GetFullName();
    boolean SetEncoding(final String en);
}
