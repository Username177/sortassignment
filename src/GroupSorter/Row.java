package GroupSorter;


public interface Row {
    String GetValue(int position);
    boolean HasErrors();
    int Size();
    String ToString();
}
