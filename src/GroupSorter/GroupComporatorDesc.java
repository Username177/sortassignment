package GroupSorter;


import java.util.Comparator;

public class GroupComporatorDesc implements Comparator {

    @Override
    public int compare(Object o1, Object o2) {
        Integer size1 = ((Group)o1).Size();
        Integer size2 = ((Group)o2).Size();
        
        return size2.compareTo(size1) ;
    }
}
