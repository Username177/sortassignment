package GroupSorter;



import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

public class FileLinear implements File {
    private String              fileName;
    private String              encoding;
    private java.io.File        file;
    private OutputStreamWriter  bfos;
    private BufferedReader      bfis;
    private boolean             isSetForReading = false;
    private boolean             isSetForWriting = false;

    @Override
    public boolean OpenForReading(final String fileName) {
        this.fileName = fileName;
        String filePath;

        if(encoding == null) {
            encoding = Charset.defaultCharset().toString();
        }
        
        if(fileName == null) {
            return false;
        }

        filePath = fileName;

        try {
            file = new java.io.File(filePath);
            bfis = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding));
        }
        catch (IOException e) {
            return false;
        }
        isSetForReading = true;
        return true;
    }

    @Override
    public boolean OpenForWriting(final String fileName, boolean append) {
        this.fileName = fileName;
        String filePath;

        if(encoding == null) {
            encoding = Charset.defaultCharset().toString();
        }
        
        if(fileName == null) {
            return false;
        }

        filePath = fileName;

        try {
            file = new java.io.File(filePath);
            bfos = new OutputStreamWriter(new FileOutputStream(file, append), encoding);
        }
        catch (IOException e) {
            return false;
        }

        isSetForWriting = true;
        return true;
    }

    @Override
    public boolean Write(final String data) {
        if(!isSetForWriting) {
            return false;
        }

        try {
            bfos.write(new String(data.getBytes(encoding)));
        }
        catch (Exception e) {
            return false;
        }

        return true;
    }

    @Override
    public void Close() {
        try {
            if (isSetForReading) {
                bfis.close();
            }
            else if(isSetForWriting) {
                bfos.close();
            }
        }
        catch (IOException e) {
        }
    }

    @Override
    public String GetFileName() {
        return fileName;
    }

    @Override
    public String GetFullName(){
        return fileName;
    }

    @Override
    public long GetLength() {
        long length = file.length();
        return length;
    }

    @Override
    public String Read(int length) {
        if(!isSetForReading) {
            return null;
        }

        if(length < 1 || length > file.length()) {
            return null;
        }

        char[] buffer = new char[length];
        int bytesRead = 0;

        try {
            bytesRead = bfis.read(buffer);
        }
        catch (IOException | ArrayIndexOutOfBoundsException e) {
            return null;
        }

        if(bytesRead < 1) {
            return null;
        }
        else {
            return new String(buffer, 0, bytesRead);
        }
    }

    @Override
    public String ReadLine() {
        if(!isSetForReading) {
            return null;
        }
        
        try {
            return bfis.readLine();
        }
        catch (IOException e) {
            return null;
        }
    }
    
    @Override
    public boolean Seek(long index) {
        if(!isSetForReading || index < 1 || index > file.length()) {
            return false;
        }

        try {
            bfis.mark(0);
            bfis.reset();
            bfis.skip(index);
        } catch (IOException e) {
            
        }


        return true;
    }

    @Override
    public boolean SetEncoding(final String encoding) {
        if(!Charset.isSupported(encoding)) {
           return false;  
        }
        
        this.encoding = encoding;
        return true;
    }
}
