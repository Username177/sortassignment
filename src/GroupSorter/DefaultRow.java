package GroupSorter;


import java.util.Arrays;

public class DefaultRow implements Row {
    private static final int CELLCOUNT = 3;
    private String originalLine;
    private String[] cells;
    private int hash;
    private int errors = 0;
    
    DefaultRow(final String line) {
        originalLine = line;
        cells = line.split(";");
        CheckValues();
        hash = Arrays.hashCode(cells);
    }    
    
    @Override
    public String GetValue(int position) {
        return cells[position];
    }
    
    @Override
    public int hashCode() {
        return hash;
    }
    
    @Override
    public int Size() {
        if(cells==null) {
            return 0;
        }
        
        return cells.length;
    }
    
    @Override
    public String ToString() {
        return originalLine;
    }
    
    @Override
    public boolean HasErrors() {
        if( errors > 0 ) {
            return true;
        }

        return false;
    }

    private void CheckValues() {
        if(cells.length != CELLCOUNT) {
            errors++;
            return;
        }
        
        int nullRows = 0;
        
        for(int i = 0; i < cells.length; i++) {
            if(cells[i] == null || cells[i].equals("\"\"")) {
                nullRows++;
                continue;
            }
        }
        
        if(nullRows == CELLCOUNT) {
            errors++;
        }
    }
    
    public static int GetCellCount() {
        return CELLCOUNT;
    }

    @Override
    public boolean equals(final Object o) {
        if(o.getClass() != this.getClass()) {
            return false;
        }
        
        Row row = (Row)o;
        
        if(row.Size() != CELLCOUNT) {
            return false;
        }
        
        for(int i = 0; i < CELLCOUNT; i++) {
            String rowValue = row.GetValue(i);
            
            if( cells[i] == null && rowValue != null) {
                return false;
            }
            
            if( cells[i] == null && rowValue == null) {
                continue;
            }
            
            if(!cells[i].equals(rowValue)) {
                return false;
            }
        }
        
        return true;
    }
}
