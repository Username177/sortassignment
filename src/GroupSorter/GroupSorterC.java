package GroupSorter;


import java.io.InvalidClassException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GroupSorterC implements GroupSorter {
    private static final String DESCTIPTION = "Сортировка по группам. Версия «С».";
    private int nCells = 3;
    private Set<Row>  uniqueRows;
    private Map<Integer, Group> groups = new HashMap<>();
    private Map<String, Integer>[] pointers; 
    private int nGroups;
    
    /*
     * Копирует список проверяя элементы на наличие ошибок и удаляя дубликаты.
     */
    @Override
    public void SetArray(final List<Row> list, int nRows) {
        if(list == null || list.isEmpty() || nRows < 1) {
            return;
        }
        
        this.nCells = nRows;
        CopyList(list);
    }

   /*
    * Алгоритм последовательно перебирает строки. Для каждой строки возможны три
    * ситуации: нет группы содержащей значения строки, есть одна или несколько
    * групп. В первом случае создается новая группа, во втором строка добавляется
    * в уже существующу, в третьем выполняется слияние групп.
    * Для отслеживания принадлежности к группе используются карты, где как значение
    * выступает индекс группы, а ключем является значение. Для каждого столбца 
    * своя карта. 
    */
    @Override
    public void Sort(){
        nGroups = 0;
        pointers = new HashMap[nCells];
        for(int i = 0; i < nCells; i++) {
            pointers[i] = new HashMap<>();
        }
        
        Iterator<Row> iterator = uniqueRows.iterator();
        while(iterator.hasNext()) {
            PutIntoGroup(iterator.next());
        }
    }
    
    private void PutIntoGroup(final Row row) {
        Integer groupKeys[] = new Integer[nCells];
        int nLinkedGroups = 0;
        
        // Проверяет наличие групп содержащих подходящие строки
        for(int i = 0; i < nCells; i++) {
            String value = row.GetValue(i);
            if(value != null && !value.equals("")) {
                groupKeys[i] = pointers[i].get(value);

                if(groupKeys[i] != null) {
                    nLinkedGroups++;
                }
            }
        }
        
        // Удаляет указатели на повторяющиеся группы в списке
        for(int i = 0; i < groupKeys.length; i++) {
            for(int j = i + 1; j < groupKeys.length; j++) {
                if( groupKeys[i] == null || groupKeys[j] == null ) {
                    continue;
                }
                
                if( groupKeys[i].intValue() == groupKeys[j].intValue()) {
                    groupKeys[j] = null;
                    nLinkedGroups--;
                }
            }
        }
        
        // Выполняет действие в зависимости от количества связанных групп
        Integer groupKey = null;
        if(nLinkedGroups == 0) {
            groupKey = CreateGroup(row);
        }
        else if(nLinkedGroups == 1) {
            groupKey = FindGroup(groupKeys);
        }
        else if(nLinkedGroups > 1) {
            groupKey = MergeGroups(nLinkedGroups, groupKeys, row);
        }
        
        // Добавляет строку в индекс и обновляет индекс
        Group group = groups.get(groupKey);
        group.AddRow(row);
        UpdateIndex(row, groupKey);
    }
    
    private Integer CreateGroup(final Row row) {
        Integer groupKey = this.nGroups++;
        Group group = new DefaultGroup();
        groups.put(groupKey, group);

        return groupKey;
    }
    
    // Добавляет указатель на группу для каждого значения строки
    private void UpdateIndex(final Row row, final Integer groupKey) {
        for(int i = 0; i < nCells; i++) {
            pointers[i].put(row.GetValue(i), groupKey);
        }
    }
    
    private Integer FindGroup(final Integer[] groupIDS) {
        Integer groupKey = null;
        
        // Перебирает массив до первого не нулевого значения
        for(int i = 0; i < groupIDS.length; i++) {
            if(groupIDS[i] != null) {
                groupKey = groupIDS[i];
                break;
            }
        }

        return groupKey;
    }

    private Integer MergeGroups(int count, final Integer[] groupKeys, final Row row) {
        int max = 0;
        Group biggestGroup = null;
        Integer biggestGroupKey = null;
        
        /* 
         * Находит все связанные группы и определяет группу с наибольшим
         * количеством строк. Удаляет все остальные группы из списка.
        */
        List<Group> linkedGroups = new ArrayList<>(count);
        for(int i = 0; i < groupKeys.length; i++) {
            if(groupKeys[i] == null) {
                continue;
            }
                
            Group group = groups.get(groupKeys[i]);
            linkedGroups.add(group);

            int size = group.Size();
            if(size > max) {
                groups.remove(biggestGroupKey);

                max = size;
                biggestGroup = group;
                biggestGroupKey = groupKeys[i];
            }
            else {
                groups.remove(groupKeys[i]);
            }

        }
        
        // Копирует строки в самую большую группу
        for(int i = 0; i < linkedGroups.size(); i++) {
            Group group = linkedGroups.get(i);
            
            if(group != null && biggestGroup != group) {
                try {
                    biggestGroup.Include(group);
                    
                    // Обновить индекс для каждой перемещенной строки
                    for(int k = 0; k < group.Size(); k++) {
                        UpdateIndex(group.GetRow(k), biggestGroupKey);
                    }
                } catch (InvalidClassException ex) {
                    
                }
            }
        }
        
        return biggestGroupKey;
    }

    @Override
    public Group[] GetGroups() {
        Group[] groupsArray = new Group[groups.size()];
        groups.values().toArray(groupsArray);
        
        // Сортирует массив по количеству строк
        Arrays.sort(groupsArray, new GroupComporatorDesc());
        
        return groupsArray;
    }

    @Override
    public String GetDescription() {
       return DESCTIPTION;
    }
    
    private void CopyList(final List<Row> unsortedRows) {
        int nSkipped = 0;
        uniqueRows = new HashSet<>(unsortedRows.size());
        
        List<Row> duplicateRows = new LinkedList<>();
        List<Row> skippedRows = new LinkedList<>();

        // Копирует строки в коллекцию параллельно проверяя на наличие ошибок.
        Iterator<Row> unsortedIterator = unsortedRows.iterator();
        while( unsortedIterator.hasNext()) {
            Row row = unsortedIterator.next();
            if(row.HasErrors()) {
                nSkipped++;
                skippedRows.add(row);
            }
            else if(!uniqueRows.add(row)) {
                duplicateRows.add(row);
            }
        }
        
        Utils.DebugPrint("Уникальных строк: " + String.valueOf(uniqueRows.size()));
        Utils.DebugPrint("Строк с ошибками: " + String.valueOf(nSkipped));
        
        /* Печатает строки с ошибками 
        Utils.DebugPrint("Строк с ошибками: " + String.valueOf(nSkipped));
        if(!skippedRows.isEmpty() && skippedRows.size() < 20) {
            Iterator<Row> it = skippedRows.iterator();
            while(it.hasNext()) {
                Utils.DebugPrint("Исключенная строка: " + it.next().ToString());
            }
        }
        */
        
        Utils.DebugPrint("Дубликатов: " + String.valueOf(duplicateRows.size()));

        /* Печатает дубликаты
        if( !duplicateRows.isEmpty() && duplicateRows.size() < 20) {
            Iterator<Row> duplicateIterator = duplicateRows.iterator();
            while(duplicateIterator.hasNext()) {
                Utils.DebugPrint("Дубликат:" + duplicateIterator.next().ToString());
            }
        }
        */
    }
}
