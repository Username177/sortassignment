package GroupSorter;


import java.nio.charset.Charset;
import java.util.LinkedList;
import java.util.List;

public class Sort {
    static String filePath;
    static File file;
    static List<Row> unsortedRows = new LinkedList<>();

    public static void main(String[] args) {
        //args = new String[]{"lng.csv"};
        
        /* Тестовый файл
        CreateTestFile("test.txt");
        args = new String[]{"test.txt"};
        */
        
        if( !GetFilePath(args) ){
            return;
        }
        
        if( !OpenFile() ) {
            return;
        }
        
        ReadFile();        
        
        Group[] groups = Sort(new GroupSorterC());
        
        WriteToFile(groups);
        
        EndProgram();
    }

    private static Group[] Sort(GroupSorter sorter) {
        Utils.DebugPrint(sorter.GetDescription());
        sorter.SetArray(unsortedRows, DefaultRow.GetCellCount());
        Utils.DebugPrint("Начало сортировки");
        sorter.Sort();
        Utils.DebugPrint("Завершение сортировки");
        Group[] groups = sorter.GetGroups();
        Utils.DebugPrint("Групп создано: " + String.valueOf(groups.length));

        return groups;
    }

    private static void EndProgram() {
        Utils.DebugPrint("Программа завершена.");
    }

    private static boolean GetFilePath(final String[] args) {
        // Проверить наличие аргумента
        if (args.length == 0 || args[0].length() == 0) {
            Utils.DebugPrint("Добавьте аргумент с путем к файлу.");
            return false;
        }
        filePath = args[0];
        return true;
    }

    private static boolean OpenFile() {
        // Попытаться открыть файл
        file = new FileLinear();
        boolean fileIsOpen = file.OpenForReading(filePath);
        
        if (!fileIsOpen) {
            Utils.DebugPrint("Невозможно открыть файл");
        } else {
            float fileSize = (float)file.GetLength() / 1024 / 1024;
            Utils.DebugPrint( "Файл открыт. Размер: " + String.format("%.2f", fileSize) + "мб." );
        }
        
        return fileIsOpen;
    }

    private static void ReadFile() {
        // Загрузить содержимое
        while(true) {
            String line = file.ReadLine();
            
            if( line == null ) {
                break;
            }
            
            unsortedRows.add(new DefaultRow(line));
        }
        
        Utils.DebugPrint(String.valueOf("Прочитано записей: " + unsortedRows.size()));
    }

    private static void WriteToFile(Group[] groups) {
        String endl = System.getProperty("line.separator");
        
        File out = new FileLinear();
        boolean result = out.OpenForWriting("out.txt", false);
        
        for(int i = 0; i < groups.length; i++) {
            Group group = groups[i];
            out.Write("Группа " + String.valueOf(i + 1) + ", записей: " + String.valueOf(group.Size()) + endl);
            
            Row[] rows = group.GetRows();
            
            for(int j = 0; j < group.Size(); j++) {
                out.Write(rows[j].ToString() + endl);
            }
            
            out.Write(endl);
        }
        
        out.Close();
    }

    private static void CreateTestFile(final String filePath) {
        String endl = System.getProperty("line.separator");
        
        File out = new FileLinear();
        out.OpenForWriting(filePath, false);
        
        int linkCol = 0;
        for(int i = 0; i < 1000; i++) {
            // Значение для связи строк
            String linkValue = String.valueOf(i);
            
            for(int j = 1; j < 1001; j++) {
                String[] cells = new String[3];
                
                for(int k = 0; k < cells.length; k++) {
                    // Второстепенные значения
                    cells[k] = String.valueOf(j * 1000);
                }
                cells[linkCol] = linkValue;
                
                out.Write("\"");
                for(int k = 0; k < cells.length - 1; k++) {
                    out.Write(cells[k] + "\";\"");
                }
                
                out.Write(cells[cells.length - 1] + "\"");
                out.Write(endl);
            }
            
            if(++linkCol > 2) {
                linkCol = 0;
            }
        }
        
        out.Close();
    }
}
