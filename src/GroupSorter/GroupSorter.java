package GroupSorter;


import java.util.List;

public interface GroupSorter {
    void SetArray(final List<Row> list, int nRows);
    void Sort();
    Group[] GetGroups();
    String GetDescription();
}
