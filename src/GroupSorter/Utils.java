package GroupSorter;


import java.text.SimpleDateFormat;
import java.util.Calendar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Cash
 */
public class Utils {
    
    public static void DebugPrint(final String message) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SSS");
        
        java.lang.System.out.println(sdf.format(cal.getTime()) + ": " + message);
    }
    
}
