package GroupSorter;


import java.io.InvalidClassException;

public interface Group {
    void  AddRow(Row row);
    Row   GetRow(int position);
    Row[] GetRows();
    void  RemoveRow(int position);
    int   Size();
    void  Include(Group group) throws InvalidClassException;
}
